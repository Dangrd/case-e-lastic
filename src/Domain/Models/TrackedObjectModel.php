<?php


namespace Domain\Models;


class TrackedObjectModel extends BaseModel
{
    public string $code;
    public string $status;
    public string $substatus;
    public int $mail_address;

    public function __construct($id, $code, $status, $substatus, $mail_address)
    {
        $this->id = $id;
        $this->code = $code;
        $this->status = $status;
        $this->substatus = $substatus;
        $this->mail_address = $mail_address;
    }
}