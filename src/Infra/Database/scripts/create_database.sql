CREATE DATABASE case_db;

CREATE TABLE tracked_objects (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    code VARCHAR(50) NOT NULL,
    status VARCHAR(250),
    substatus VARCHAR(250),
    mail_address VARCHAR(250)
);