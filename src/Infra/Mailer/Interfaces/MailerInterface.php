<?php


namespace Infra\Mailer\Interfaces;


interface MailerInterface
{
    public function send(string $address, string $subject, $body);
}