<?php

namespace Infra\Database\Configuration;

use PDO;

class DatabaseConfiguration
{
    public string $db_username = 'root';
    public string $db_password = '';
    public string $db_host = 'localhost';
    public string $db_name = 'case_db';
    public array $options = [
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];

    public function __construct(string $username, string $password, string $host, string $db_name)
    {
        $this->db_username = $username;
        $this->db_password = $password;
        $this->db_host = $host;
        $this->db_name = $db_name;
    }
}