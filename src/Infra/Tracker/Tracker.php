<?php


namespace Infra\Tracker;

use Infra\Tracker\Configuration\TrackerConfiguration;
use Infra\Tracker\Interfaces\TrackerInterface;

class Tracker implements TrackerInterface
{
    private TrackerConfiguration $configuration;

    public function __construct(TrackerConfiguration $configuration)
    {
        $this->configuration = $configuration;
    }

    public function track(string $code) : array
    {
        $http_client = curl_init();

        $url = $this->configuration->getEndpoint($code);

        curl_setopt_array(
            $http_client,
            array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            )
        );

        $response = curl_exec($http_client);

        curl_close($http_client);

        return json_decode($response, TRUE);
    }
}