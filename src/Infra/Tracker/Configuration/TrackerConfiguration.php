<?php


namespace Infra\Tracker\Configuration;


class TrackerConfiguration
{
    public string $base_url;
    public string $user;
    public string $token;

    public function __construct()
    {
        $this->base_url = "https://api.linketrack.com/track/json?";
        $this->user = "daniel.gmrd@gmail.com";
        $this->token = "69b26eff85d0323bb59bc7ea89cbeb811b22f746423987ef511ecea715efc704";
    }

    public function getEndpoint(string $code): string
    {
        return $this->base_url .
            "user={$this->user}&" .
            "token={$this->token}&" .
            "codigo={$code}";
    }
}