<?php


namespace Infra\Mailer;


use Infra\Mailer\Configuration\MailerConfiguration;
use Infra\Mailer\Interfaces\MailerInterface;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class Mailer implements MailerInterface
{
    private PHPMailer $mailer;
    private MailerConfiguration $configuration;

    public function __construct(PHPMailer $mailer, MailerConfiguration $configuration)
    {
        $this->mailer = $mailer;
        $this->configuration = $configuration;
    }

    public function send(string $address, string $subject, $body)
    {
        try {
            $this->mailer->setFrom($this->configuration->sender_address, $this->configuration->sender_name);
            $this->mailer->addAddress($address);
            $this->mailer->isHTML(TRUE);
            $this->mailer->Subject = $subject;
            $this->mailer->Body = $this->prepareHTML($body);
            $this->mailer->AltBody = $body;
            $this->mailer->send();
        } catch (Exception $e) {
            //tratar
        }
    }

    private function prepareHTML($content): string
    {
        $prepared_HTML = file_get_contents("../public/mail/mail.html");

        $event_section_HTML = $this->prepareEventSectionHTML($content['eventos']);

        $prepared_HTML = str_replace('{EVENT_SECTION}', $event_section_HTML, $prepared_HTML);

        return $prepared_HTML;
    }

    private function prepareEventSectionHTML(array $events): string
    {
        $prepared_HTML = "";

        foreach($events as $e)
        {
            $section_HTML = file_get_contents("../public/mail/mail_events_section.html");

            $section_HTML = str_replace('{EVENT_INFO_DATE}', $e['data'], $section_HTML);
            $section_HTML = str_replace('{EVENT_INFO_TIME}', $e['hora'], $section_HTML);
            $section_HTML = str_replace('{EVENT_INFO_LOCATION}', $e['local'], $section_HTML);
            $section_HTML = str_replace('{EVENT_STATUS}', $e['status'], $section_HTML);
            $section_HTML = str_replace('{EVENT_SUBSTATUS}', $e['subStatus'], $section_HTML);

            $prepared_HTML .= $section_HTML;
        }

        return $prepared_HTML;
    }
}