<?php


namespace Infra\Database;


use Exception;
use Infra\Database\Configuration\DatabaseConfiguration;
use PDO;
use PDOException;

class Database
{
    private DatabaseConfiguration $configuration;

    public function __construct(DatabaseConfiguration $configuration)
    {
        $this->configuration = $configuration;
    }

    private function connect()
    {
        try {
            $connection = new PDO(
                'mysql:host=' . $this->configuration->db_host . ';dbname=' . $this->configuration->db_name,
                $this->configuration->db_username,
                $this->configuration->db_password,
                $this->configuration->options
            );
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $connection;
        }
        catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return null;
        }
    }

    public function write($sql): bool
    {
        $connection = $this->connect();

        $statement = $connection->prepare($sql);

        return $statement->execute();
    }

    public function read($sql): array
    {
        $connection = $this->connect();

        $statement = $connection->prepare($sql);

        if($statement->execute())
            return $statement->fetchAll();

        return null;
    }
}