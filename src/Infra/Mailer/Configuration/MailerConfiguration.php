<?php


namespace Infra\Mailer\Configuration;


class MailerConfiguration
{
    public string $sender_name;
    public string $sender_address;

    public function __construct()
    {
        $this->sender_name = "Daniel Rodrigues";
        $this->sender_address = "daniel.gmrd@gmail.com";
    }
}