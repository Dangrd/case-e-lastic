<?php


namespace Infra\Database\Repositories;


use Domain\Models\TrackedObjectModel;
use Infra\Database\Database;
use Infra\Database\Repositories\Interfaces\TrackedObjectRepositoryInterface;

class TrackedObjectRepository extends BaseRepository implements TrackedObjectRepositoryInterface
{
    public function __construct(Database $database)
    {
        $this->table = 'tracked_objects';

        parent::__construct($database);
    }

    public function create(string $code, $mail_address, string $status = "", string $substatus = ""): bool
    {
        $sql = "INSERT INTO '{$this->table}' (`code`,`status`, `substatus`, `mail_address`) 
        VALUES ('{$code}','{$status}','{$substatus}','{$mail_address}')";

        return $this->database->write($sql);
    }

    public function getByCode(string $code): TrackedObjectModel
    {
        $sql = "SELECT * FROM '{$this->table}' WHERE `code` = '{$code}'";

        $result = $this->database->read($sql);

        return new TrackedObjectModel(
            $result['id'],
            $result['code'],
            $result['status'],
            $result['substatus'],
            $result['mail_address']
        );
    }

    public function getAll(): array
    {
        $sql = "SELECT * FROM '{$this->table}'";

        return $this->database->read($sql);
    }

    public function getByMailAddress($mail_address)
    {
        $sql = "SELECT * FROM '{$this->table}' WHERE mail_address = '{$mail_address}'";

        return $this->database->read($sql);
    }
}