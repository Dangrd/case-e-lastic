<?php


namespace Application\Services\Interfaces;


interface ServiceInterface
{
    public function trackAndNotify(string $customer_email, string $object_code);

    public function notifyStatusChange();
}