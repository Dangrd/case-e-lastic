<?php


namespace Infra\Database\Repositories;


use Infra\Database\Database;

abstract class BaseRepository
{
    protected string $table;
    protected Database $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }
}