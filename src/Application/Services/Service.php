<?php


namespace Application\Services;


use Application\Services\Interfaces\ServiceInterface;
use Infra\Database\Repositories\Interfaces\TrackedObjectRepositoryInterface;
use Infra\Mailer\Interfaces\MailerInterface;
use Infra\Tracker\Interfaces\TrackerInterface;

class Service implements ServiceInterface
{
    private TrackedObjectRepositoryInterface $object_repository;
    private TrackerInterface $tracker;
    private MailerInterface $mailer;

    /**
     * Service constructor.
     * @param TrackedObjectRepositoryInterface $object_repository
     * @param TrackerInterface $tracker
     * @param MailerInterface $mailer
     */
    public function __construct(
        TrackedObjectRepositoryInterface $object_repository,
        TrackerInterface $tracker,
        MailerInterface $mailer
    )
    {
        $this->object_repository = $object_repository;
        $this->tracker = $tracker;
        $this->mailer = $mailer;
    }

    /**
     * Adds a new object and sends an email about it's current status
     *
     * @param string $customer_email
     * @param string $object_code
     */
    public function trackAndNotify(string $customer_email, string $object_code)
    {
        $object_trace = $this->tracker->track($object_code);

        $this->object_repository->create(
            $object_code,
            $customer_email,
            $object_trace['eventos'][0]['status'],
            $object_trace['eventos'][0]['substatus']
        );

        $this->mailer->send($customer_email, $object_trace['eventos'][0]['status'], $object_trace);
    }

    // This method should be called by a scheduler
    public function notifyStatusChange()
    {
        $tracked_objs = $this->object_repository->getAll();

        foreach($tracked_objs as $obj)
        {
            $object_trace = $this->tracker->track($obj['code']);

            if($obj['status'] !== $object_trace['eventos'][0]['status'])
            {
                $this->mailer->send($obj['mail_address'], $object_trace['eventos'][0]['status'], $object_trace);
            }
        }
    }
}