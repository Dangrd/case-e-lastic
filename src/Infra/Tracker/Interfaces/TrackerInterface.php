<?php


namespace Infra\Tracker\Interfaces;


interface TrackerInterface
{
    public function track(string $code);
}