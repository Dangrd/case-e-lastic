<?php


namespace Infra\Database\Repositories\Interfaces;


use Domain\Models\TrackedObjectModel;

interface TrackedObjectRepositoryInterface
{
    public function create(string $code, $mail_address, string $status = "", string $substatus  = ""): bool;

    public function getByCode(string $code): TrackedObjectModel;

    public function getAll(): array;

    public function getByMailAddress($mail_address);
}